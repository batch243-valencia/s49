// console.log("Go Elliot");
//fetch()
//This is a method in JavaSasdasdacript that is used to send request in the server and load the information (response from the server) in the webpage.
//Syntax:
//fetch ("urlAPI"), {optional/resquest from the user and response to the user})
// API: https://jsonplaceholder.typicode.com/posts
// let fetchSpecificPosts = (id) =>{
// 	if(id <= 100 && id >= 1){
// 		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
// 		.then(response => response.json()).then(data => console.log(data));
// 	}else{
// 		console.log("Invalid ID");
// 	}
// }
// fetchSpecificPosts(5);
let fetchPosts = () => {
    fetch("https://jsonplaceholder.typicode.com/posts")
        .then(response => response.json()).then(data => {
            return showPosts(data);
        });
}
fetchPosts();
const showPosts = (posts) => {
    let postEntries = ``;
    posts.forEach((post) => {
        postEntries += `
		<div id = "post-${post.id}"> 
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick = "editPost('${post.id}')">Edit</button>
			<button onclick = "deletePost('${post.id}')">Delete</button
		</div>
		`
    });
    document.querySelector("#div-post-entries").innerHTML = postEntries;
}
//Add Posts data
document.querySelector(`#form-add-post`).addEventListener("submit", (event) => {
    event.preventDefault();
    let title = document.querySelector(`#txt-title`).value;
    let body = document.querySelector(`#txt-body`).value;
    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title: title,
            body: body,
            userId: 1
        }),
        headers: {
            "Content-Type": "application/json"
        }
    }).then(response => response.json())
        .then(data => {
            console.log(data);
            alert("Successfully Added");
            let title = document.querySelector(`#txt-title`) = "";
            let body = document.querySelector(`#txt-body`) = "";
        });
});
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#btn-submit-update').removeAttribute('disabled');
}
document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
    event.preventDefault();
    let id = document.querySelector("#txt-edit-id").value;
    let title = document.querySelector("#txt-edit-title").value;
    let body = document.querySelector("#txt-edit-body").value;
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: "PATCH",
        body: JSON.stringify({
            title: title,
            body: body
        }),
        headers: {
            "Content-Type": "application/json"
        }
    }).then(response => response.json())
        .then(data => {
            console.log(data);
            alert("Successfully Updated");
            document.querySelector("#txt-edit-id").value = null;
            document.querySelector("#txt-edit-title").value = null;
            document.querySelector("#txt-edit-body").value = null;
            document.querySelector("#btn-submit-update").setAttribute("disabled", true);
        })
});
const deletePost = (id) => {
    // console.log("Hi I'm from delete function " + id);
    fetch(`https://jsonplaceholder.typicode.com/posts/${id - 1}`, { method: "DELETE" }).then(response => response.json()).then(data => alert("Post has been deleted"));
    document.querySelector(`#post-${id}`).remove();
}